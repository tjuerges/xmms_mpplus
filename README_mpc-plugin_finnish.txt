--------------------- .mpc Musepack LUEMINUT -----------------------

Olet siis p��tt�nyt ladata Musepack pluginin WinAMP soittimeen.
Jos sinulla on ehdotuksia tai l�yd�t vikoja, l�het� s�hk�postia
(englanniksi tai saksaksi) osoitteeseen

    Andree.Buschmann@web.de


.mpc Musepack (tm)  on Andree Buschmannin rekister�im� tavaramerkki

--------------------------------------------------------------------

ASENTAMINEN
~~~~~~~~~~~

XMMS

    Kopioi tiedosto "xmms-musepack-*.dll" XMMSin hakemistossa olevaan
    alihakemistoon /usr/X11/lib/xmms/Input ja k�ynnist� XMMS uudelleen. XMMS
    ottaa pluginin automaattisesti k�ytt��n.


--------------------------------------------------------------------

Usein kysytyt kysymykset (UKK / FAQ)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

K1: MPC tiedostot kuulostavat tunkkaisemmilta kuin alkuper�iset
    WAVE-��nitedostot verrattaessa WinAMPilla!
V1: Varmista pluginin asetuksista ett� "S�r�ytymissuoja" ja "Replaygain"
    ominaisuudet ovat poissa k�yt�st�.

K2: ID3v2-tagia ei tunnisteta!
V2: Plugin ohittaa tiedot tarkoituksella. Soitto ja kelaus toimii silti
    normaalisti.

K3: Kelaaminen on paljon hitaampaa kuin MP3 tiedostoissa!
V3: MPC plugini joutuu k�ym��n koko tiedoston l�pi freimi kerrallaan. Siit�
    johtuen kelaamisnopeus on riippuvainen koneen kiintolevyn ja prosessorin
    nopeudesta.

K4: Mit� "dither�inti" tekee?
V4: Purkajan ulostulosignaali t�ytyy kvantisoida 16 bitin tarkkuuteen.
    Normaalisti t�m� tehd��n liukulukuja kokonaisluvuiksi muutettaessa joko
    j�tt�m�ll� ylim��r�iset bitit huomiotta tai py�rist�m�ll� arvot. N�m�
    tavat aiheuttavat harmonista s�r��. S�r�n est�miseksi liukulukuarvoihin
    lis�t��n hiljaista kohinaa ennen py�rist�mist� 16 bitin tarkkuuteen.
    T�m� on "dither�inti�".


--------------------------------------------------------------------
