#ifndef _huff_new_h_
#define _huff_new_h

#include "bitstream.h"

/* P R O Z E D U R E N */
void Huffman_SV7_Encoder(void);
void Huffman_SV7_Decoder(void);

/* V A R I A B L E N */
extern HuffmanTyp HuffHdr[10];
extern HuffmanTyp HuffSCFI[4];
extern HuffmanTyp HuffDSCF[16];
extern const HuffmanTyp* HuffQ[2][8];

#endif
