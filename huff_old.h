#ifndef _huff_old_h_
#define _huff_old_h_

#include "bitstream.h"

/* F U N K T I O N E N */
void Huffman_SV6_Encoder(void);
void Huffman_SV6_Decoder(void);

/* V A R I A B L E N */
extern const HuffmanTyp *SampleHuff[18];
extern HuffmanTyp SCFI_Bundle[8],DSCF_Entropie[13];
extern HuffmanTyp Region_A[16],Region_B[8],Region_C[4];

#endif
