#define DECODER
#define SYNTH_DELAY 481

#include <math.h>
#include <string.h>
#include "mpc_dec.h"
#include "requant.h"
#include "huff_old.h"
#include "huff_new.h"
#include "synth_filter.h"
#include "bitstream.h"

/* D E C L A R A T I O N S */
typedef struct {int L[36]; int R[36];} QuantTyp;

/* F U N C T I O N S */
void Requantisierung(const int);
void perform_EQ(void);
void Intensity_Stereo_Decode(void);

/* V A R I A B L E S */
int SCF_Index_L[32][3],SCF_Index_R[32][3];      // Skalenfaktor-Indices fuer Speicherung
QuantTyp Q[32];                                 // quantisierte Samples
int Res_L[32],Res_R[32];                        // Aufloesungsstufen der Teilbaender
int DSCF_Flag_L[32],DSCF_Flag_R[32];            // Flag bei Benutzung von differential-SCF
int SCFI_L[32],SCFI_R[32];                      // Uebertragungsreihenfolge der SCF
int DSCF_Reference_L[32],DSCF_Reference_R[32];  // enthaelt letzten SCF des letzten Frames
int MS_Flag[32];                                // subbandweiser Flag fuer M/S-Signalfuehrung

// globale Variablen fuer EQ
float EQ_gain[32-FIR_BANDS];
float EQ_Filter[FIR_BANDS][EQ_TAP];
unsigned int EQ_activated = 0;

// weitere globale Flags
unsigned int MS_used;
unsigned int StreamVersion;
unsigned int FwdJumpInfo;
unsigned int ActDecodePos;
//int FrameWasValid;
unsigned int OverallFrames;
unsigned int DecodedFrames;


extern int TrueGapless;


/* F U N C T I O N S */
void RESET_Synthesis(void)
{
    Reset_V();
}

void RESET_Globals(void)
{
    Reset_BitstreamDecode();
    DecodedFrames = 0;
    StreamVersion = MS_used = 0;
    memset(Y_L             ,0, sizeof(Y_L));
    memset(Y_R             ,0, sizeof(Y_R));
    memset(SCF_Index_L     ,0, sizeof(SCF_Index_L));
    memset(SCF_Index_R     ,0, sizeof(SCF_Index_R));
    memset(Res_L           ,0, sizeof(Res_L));
    memset(Res_R           ,0, sizeof(Res_R));
    memset(SCFI_L          ,0, sizeof(SCFI_L));
    memset(SCFI_R          ,0, sizeof(SCFI_R));
    memset(DSCF_Flag_L     ,0, sizeof(DSCF_Flag_L));
    memset(DSCF_Flag_R     ,0, sizeof(DSCF_Flag_R));
    memset(DSCF_Reference_L,0, sizeof(DSCF_Reference_L));
    memset(DSCF_Reference_R,0, sizeof(DSCF_Reference_R));
    memset(Q               ,0, sizeof(Q));
    memset(MS_Flag         ,0, sizeof(MS_Flag));
}

void perform_EQ(void)
{
    static float SAVE_L[DELAY][32];             // buffer for..
    static float SAVE_R[DELAY][32];             // ..upper subbands
    static float FirSave_L[FIR_BANDS][EQ_TAP];  // buffer for..
    static float FirSave_R[FIR_BANDS][EQ_TAP];  // ..lowest subbands
    float lowestSB_L[FIR_BANDS][36],lowestSB_R[FIR_BANDS][36];
    float SWAP[DELAY][32],x[36];
    int n,k,i;

    //L: delay subbands
    for (i=0; i<FIR_BANDS; ++i) for (k=0; k<36; ++k) lowestSB_L[i][k] = Y_L[k][i];
    memcpy(SWAP,       SAVE_L,       DELAY     *32*sizeof(float));
    memcpy(SAVE_L,     Y_L+36-DELAY, DELAY     *32*sizeof(float));
    memmove(Y_L+DELAY, Y_L,          (36-DELAY)*32*sizeof(float));
    memcpy(Y_L,        SWAP,         DELAY     *32*sizeof(float));

    //R: delay subbands
    for (i=0; i<FIR_BANDS; ++i) for (k=0; k<36; ++k) lowestSB_R[i][k] = Y_R[k][i];
    memcpy(SWAP,       SAVE_R,       DELAY     *32*sizeof(float));
    memcpy(SAVE_R,     Y_R+36-DELAY, DELAY     *32*sizeof(float));
    memmove(Y_R+DELAY, Y_R,          (36-DELAY)*32*sizeof(float));
    memcpy(Y_R,        SWAP,         DELAY     *32*sizeof(float));

    // apply global EQ to upper subbands
    for (k=0; k<36; ++k)
    {
        for (n=FIR_BANDS; n<=Max_Band; ++n)
        {
            Y_L[k][n] *= EQ_gain[n-FIR_BANDS];
            Y_R[k][n] *= EQ_gain[n-FIR_BANDS];
        }
    }
    // apply FIR to lower subbands for each channel
    for (i=0; i<FIR_BANDS; ++i)
    {
        // L: perform filter for lowest subbands
        for (k=0; k<36; ++k)
        {
            x[k] = 0;
            for (n=0; n<EQ_TAP; ++n)
            {
                if (k+n>=EQ_TAP) x[k] += lowestSB_L[i][k+n-EQ_TAP]* EQ_Filter[i][n];
                else             x[k] += FirSave_L[i][k+n]        * EQ_Filter[i][n];
            }
        }
        for (n=0; n<EQ_TAP; ++n) FirSave_L[i][n] = lowestSB_L[i][36-EQ_TAP+n];
        for (n=0; n<36; ++n)     Y_L[n][i] = x[n];

        // R: perform filter for lowest subbands
        for (k=0; k<36; ++k)
        {
            x[k] = 0;
            for (n=0; n<EQ_TAP; ++n)
            {
                if (k+n>=EQ_TAP) x[k] += lowestSB_R[i][k+n-EQ_TAP]* EQ_Filter[i][n];
                else             x[k] += FirSave_R[i][k+n]        * EQ_Filter[i][n];
            }
        }
        for (n=0; n<EQ_TAP; ++n) FirSave_R[i][n] = lowestSB_R[i][36-EQ_TAP+n];
        for (n=0; n<36; ++n)     Y_R[n][i] = x[n];
    }
}

extern unsigned short*  SeekTable;


int DECODE(char *buffer, int *FrameWasValid)
{
    unsigned int FrameBitCnt = 0;

    if ( DecodedFrames >= OverallFrames )               // 0...OverallFrames-1
        return 0;                                       // Abbruch: Fileende

    // read jump-info for validity check of frame
    FwdJumpInfo = Bitstream_read(20);
    SeekTable [DecodedFrames] = 20 + FwdJumpInfo;       // ...

    ActDecodePos = (Zaehler<<5) + pos;

    // decode data and check for validity of frame
    FrameBitCnt = BitsRead();
    if   (StreamVersion>=7) Lese_Bitstrom_SV7();
    else                    Lese_Bitstrom_SV6();
    *FrameWasValid = BitsRead() - FrameBitCnt == FwdJumpInfo;

    // synthesize signal
    Requantisierung(Max_Band);
    if (EQ_activated) perform_EQ();
    Synthese_Filter_opt((short*)buffer);

    ++DecodedFrames;

    // cut off first SYNTH_DELAY zero-samples
    if ( DecodedFrames == 1 ) {                         // 1...OverallFrames
        memmove ( buffer, buffer + 4*SYNTH_DELAY, 4*(1152-SYNTH_DELAY) );
        return 4 * (1152 - SYNTH_DELAY);
    }

    if ( DecodedFrames == OverallFrames  &&  StreamVersion>=7 ) {               // last frame
        int  lastsamples = Bitstream_read (11);
        if (lastsamples==0) lastsamples = 1152;
        lastsamples += SYNTH_DELAY;
        if ( TrueGapless && lastsamples > 1152 ) {
            FwdJumpInfo = Bitstream_read(20);
            FrameBitCnt = BitsRead();
            Lese_Bitstrom_SV7();
            *FrameWasValid &= BitsRead() - FrameBitCnt == FwdJumpInfo;
        }
        else {
            // memset ();
        }
        Requantisierung(Max_Band);
        if (EQ_activated) perform_EQ();
        Synthese_Filter_opt(((short*)buffer) + 2*1152);
        return 4 * lastsamples;
    }

    return 4 * 1152;
}


void Requantisierung(const int Last_Band)
{
    int Band,n;
    float facL,facR;
    float templ,tempr;
    float *YL;
    float *YR;
    int *L;
    int *R;

    // Requantisierung und Skalierung der Subband- und Subframesamples
    for (Band=0; Band<=Last_Band; ++Band)
    {
        // setting pointers
        YL = Y_L[0] + Band;
        YR = Y_R[0] + Band;
        L = Q[Band].L;
        R = Q[Band].R;
        /************************** MS-coded **************************/
        if (MS_Flag[Band])
        {
            if (Res_L[Band])
            {
                if (Res_R[Band])    // M!=0, S!=0
                {
                    facL = Cc[Res_L[Band]] * SCF[SCF_Index_L[Band][0]];
                    facR = Cc[Res_R[Band]] * SCF[SCF_Index_R[Band][0]];
                    for (n=0; n<12; ++n, YL+=32, YR+=32)
                    {
                        templ = *(L++) * facL;
                        tempr = *(R++) * facR;
                        *YL   = (templ+tempr);
                        *YR   = (templ-tempr);
                    }
                    facL = Cc[Res_L[Band]] * SCF[SCF_Index_L[Band][1]];
                    facR = Cc[Res_R[Band]] * SCF[SCF_Index_R[Band][1]];
                    for (n=0; n<12; ++n, YL+=32, YR+=32)
                    {
                        templ = *(L++) * facL;
                        tempr = *(R++) * facR;
                        *YL   = (templ+tempr);
                        *YR   = (templ-tempr);
                    }
                    facL = Cc[Res_L[Band]] * SCF[SCF_Index_L[Band][2]];
                    facR = Cc[Res_R[Band]] * SCF[SCF_Index_R[Band][2]];
                    for (n=0; n<12; ++n, YL+=32, YR+=32)
                    {
                        templ = *(L++) * facL;
                        tempr = *(R++) * facR;
                        *YL   = (templ+tempr);
                        *YR   = (templ-tempr);
                    }
                } else {    // M!=0, S==0
                    facL = Cc[Res_L[Band]] * SCF[SCF_Index_L[Band][0]];
                    for (n=0; n<12; ++n, YL+=32, YR+=32)
                    {
                        *YL = *(L++) * facL;
                        *YR = *YL;
                    }
                    facL = Cc[Res_L[Band]] * SCF[SCF_Index_L[Band][1]];
                    for (n=0; n<12; ++n, YL+=32, YR+=32)
                    {
                        *YL = *(L++) * facL;
                        *YR = *YL;
                    }
                    facL = Cc[Res_L[Band]] * SCF[SCF_Index_L[Band][2]];
                    for (n=0; n<12; ++n, YL+=32, YR+=32)
                    {
                        *YL = *(L++) * facL;
                        *YR = *YL;
                    }
                }
            } else {
                if (Res_R[Band])    // M==0, S!=0
                {
                    facR = Cc[Res_R[Band]] * SCF[SCF_Index_R[Band][0]];
                    for (n=0; n<12; ++n, YL+=32, YR+=32)
                    {
                        *YL = *(R++) * facR;
                        *YR = -(*YL);
                    }
                    facR = Cc[Res_R[Band]] * SCF[SCF_Index_R[Band][1]];
                    for (n=0; n<12; ++n, YL+=32, YR+=32)
                    {
                        *YL = *(R++) * facR;
                        *YR = -(*YL);
                    }
                    facR = Cc[Res_R[Band]] * SCF[SCF_Index_R[Band][2]];
                    for (n=0; n<12; ++n, YL+=32, YR+=32)
                    {
                        *YL = *(R++) * facR;
                        *YR = -(*YL);
                    }
                } else {    // M==0, S==0
                    for (n=0; n<36; ++n, YL+=32, YR+=32)
                    {
                        *YL = 0.0f;
                        *YR = 0.0f;
                    }
                }
            }
        }
        /************************** LR-coded **************************/
        else
        {
            if (Res_L[Band])
            {
                if (Res_R[Band])    // L!=0, R!=0
                {
                    facL = Cc[Res_L[Band]] * SCF[SCF_Index_L[Band][0]];
                    facR = Cc[Res_R[Band]] * SCF[SCF_Index_R[Band][0]];
                    for (n=0; n<12; ++n, YL+=32, YR+=32)
                    {
                        *YL = *(L++) * facL;
                        *YR = *(R++) * facR;
                    }
                    facL = Cc[Res_L[Band]] * SCF[SCF_Index_L[Band][1]];
                    facR = Cc[Res_R[Band]] * SCF[SCF_Index_R[Band][1]];
                    for (n=0; n<12; ++n, YL+=32, YR+=32)
                    {
                        *YL = *(L++) * facL;
                        *YR = *(R++) * facR;
                    }
                    facL = Cc[Res_L[Band]] * SCF[SCF_Index_L[Band][2]];
                    facR = Cc[Res_R[Band]] * SCF[SCF_Index_R[Band][2]];
                    for (n=0; n<12; ++n, YL+=32, YR+=32)
                    {
                        *YL = *(L++) * facL;
                        *YR = *(R++) * facR;
                    }
                } else {     // L!=0, R==0
                    facL = Cc[Res_L[Band]] * SCF[SCF_Index_L[Band][0]];
                    for (n=0; n<12; ++n, YL+=32, YR+=32)
                    {
                        *YL = *(L++) * facL;
                        *YR = 0.0f;
                    }
                    facL = Cc[Res_L[Band]] * SCF[SCF_Index_L[Band][1]];
                    for (n=0; n<12; ++n, YL+=32, YR+=32)
                    {
                        *YL = *(L++) * facL;
                        *YR = 0.0f;
                    }
                    facL = Cc[Res_L[Band]] * SCF[SCF_Index_L[Band][2]];
                    for (n=0; n<12; ++n, YL+=32, YR+=32)
                    {
                        *YL = *(L++) * facL;
                        *YR = 0.0f;
                    }
                }
            }
            else
            {
                if (Res_R[Band])    // L==0, R!=0
                {
                    facR = Cc[Res_R[Band]] * SCF[SCF_Index_R[Band][0]];
                    for (n=0; n<12; ++n, YL+=32, YR+=32)
                    {
                        *YL = 0.0f;
                        *YR = *(R++) * facR;
                    }
                    facR = Cc[Res_R[Band]] * SCF[SCF_Index_R[Band][1]];
                    for (n=0; n<12; ++n, YL+=32, YR+=32)
                    {
                        *YL = 0.0f;
                        *YR = *(R++) * facR;
                    }
                    facR = Cc[Res_R[Band]] * SCF[SCF_Index_R[Band][2]];
                    for (n=0; n<12; ++n, YL+=32, YR+=32)
                    {
                        *YL = 0.0f;
                        *YR = *(R++) * facR;
                    }
                } else {    // L==0, R==0
                    for (n=0; n<36; ++n, YL+=32, YR+=32)
                    {
                        *YL = 0.0f;
                        *YR = 0.0f;
                    }
                }
            }
        }
    }
}

/****************************************** SV 6 ******************************************/
void Lese_Bitstrom_SV6(void)
{
    int n,k;
    int Max_used_Band=0;
    HuffmanTyp *Table;
    const HuffmanTyp *x1;
    const HuffmanTyp *x2;
    int *L;
    int *R;
    int *ResL = Res_L;
    int *ResR = Res_R;

    /************************ HEADER **************************/
    ResL = Res_L;
    ResR = Res_R;
    for (n=0; n<=Max_Band; ++n, ++ResL, ++ResR)
    {
        if      (n<11)           Table = Region_A;
        else if (n>=11 && n<=22) Table = Region_B;
        else /*if (n>=23)*/      Table = Region_C;

        *ResL = Q_res[n][Huffman_Decode(Table)];
        if (MS_used)      MS_Flag[n] = Bitstream_read(1);
        *ResR = Q_res[n][Huffman_Decode(Table)];

        //fuehre nachfolgende Operationen nur bis zum maximal enthaltenen Subband aus
        if (*ResL!=0 || *ResR!=0) Max_used_Band = n;
    }

    /************************* SCFI-Bundle *****************************/
    ResL = Res_L;
    ResR = Res_R;
    for (n=0; n<=Max_used_Band; ++n, ++ResL, ++ResR)
    {
        if (*ResL>0) SCFI_Bundle_read(SCFI_Bundle, &SCFI_L[n], &DSCF_Flag_L[n]);
        if (*ResR>0) SCFI_Bundle_read(SCFI_Bundle, &SCFI_R[n], &DSCF_Flag_R[n]);
    }

    /***************************** SCFI ********************************/
    ResL = Res_L;
    ResR = Res_R;
    L    = SCF_Index_L[0];
    R    = SCF_Index_R[0];
    for (n=0; n<=Max_used_Band; ++n, ++ResL, ++ResR, L+=3, R+=3)
    {
        if (*ResL>0)
        {
            /*********** DSCF ************/
            if (DSCF_Flag_L[n]==1)
            {
                switch (SCFI_L[n])
                {
                case 3:
                    L[0] = DSCF_Reference_L[n] + Huffman_Decode_fast(DSCF_Entropie);
                    L[1] = L[0];
                    L[2] = L[1];
                    break;
                case 1:
                    L[0] = DSCF_Reference_L[n] + Huffman_Decode_fast(DSCF_Entropie);
                    L[1] = L[0]                + Huffman_Decode_fast(DSCF_Entropie);
                    L[2] = L[1];
                    break;
                case 2:
                    L[0] = DSCF_Reference_L[n] + Huffman_Decode_fast(DSCF_Entropie);
                    L[1] = L[0];
                    L[2] = L[1]                + Huffman_Decode_fast(DSCF_Entropie);
                    break;
                default:
                    L[0] = DSCF_Reference_L[n] + Huffman_Decode_fast(DSCF_Entropie);
                    L[1] = L[0]                + Huffman_Decode_fast(DSCF_Entropie);
                    L[2] = L[1]                + Huffman_Decode_fast(DSCF_Entropie);
                    break;
                }
            }
            /************ SCF ************/
            else
            {
                switch (SCFI_L[n])
                {
                case 3:
                    L[0] = Bitstream_read(6);
                    L[1] = L[0];
                    L[2] = L[1];
                    break;
                case 1:
                    L[0] = Bitstream_read(6);
                    L[1] = Bitstream_read(6);
                    L[2] = L[1];
                    break;
                case 2:
                    L[0] = Bitstream_read(6);
                    L[1] = L[0];
                    L[2] = Bitstream_read(6);
                    break;
                default:
                    L[0] = Bitstream_read(6);
                    L[1] = Bitstream_read(6);
                    L[2] = Bitstream_read(6);
                    break;
                }
            }
            // update Reference for DSCF
            DSCF_Reference_L[n] = L[2];
        }
        if (*ResR>0)
        {
            /*********** DSCF ************/
            if (DSCF_Flag_R[n]==1)
            {
                switch (SCFI_R[n])
                {
                case 3:
                    R[0] = DSCF_Reference_R[n] + Huffman_Decode_fast(DSCF_Entropie);
                    R[1] = R[0];
                    R[2] = R[1];
                    break;
                case 1:
                    R[0] = DSCF_Reference_R[n] + Huffman_Decode_fast(DSCF_Entropie);
                    R[1] = R[0]                + Huffman_Decode_fast(DSCF_Entropie);
                    R[2] = R[1];
                    break;
                case 2:
                    R[0] = DSCF_Reference_R[n] + Huffman_Decode_fast(DSCF_Entropie);
                    R[1] = R[0];
                    R[2] = R[1]                + Huffman_Decode_fast(DSCF_Entropie);
                    break;
                default:
                    R[0] = DSCF_Reference_R[n] + Huffman_Decode_fast(DSCF_Entropie);
                    R[1] = R[0]                + Huffman_Decode_fast(DSCF_Entropie);
                    R[2] = R[1]                + Huffman_Decode_fast(DSCF_Entropie);
                    break;
                }
            }
            /************ SCF ************/
            else
            {
                switch (SCFI_R[n])
                {
                case 3:
                    R[0] = Bitstream_read(6);
                    R[1] = R[0];
                    R[2] = R[1];
                    break;
                case 1:
                    R[0] = Bitstream_read(6);
                    R[1] = Bitstream_read(6);
                    R[2] = R[1];
                    break;
                case 2:
                    R[0] = Bitstream_read(6);
                    R[1] = R[0];
                    R[2] = Bitstream_read(6);
                    break;
                default:
                    R[0] = Bitstream_read(6);
                    R[1] = Bitstream_read(6);
                    R[2] = Bitstream_read(6);
                    break;
                }
            }
            // update Reference for DSCF
            DSCF_Reference_R[n] = R[2];
        }
    }

    /**************************** Samples ****************************/
    ResL = Res_L;
    ResR = Res_R;
    for (n=0; n<=Max_used_Band; ++n, ++ResL, ++ResR)
    {
        // setting pointers
        x1 = SampleHuff[*ResL];
        x2 = SampleHuff[*ResR];
        L = Q[n].L;
        R = Q[n].R;

        if (x1!=NULL || x2!=NULL)
            for (k=0; k<36; ++k)
            {
                if (x1!=NULL) *L++ = Huffman_Decode_fast(x1);
                if (x2!=NULL) *R++ = Huffman_Decode_fast(x2);
            }

        if (*ResL>7 || *ResR>7)
            for (k=0; k<36; ++k)
            {
                if (*ResL>7) *L++ = (int)Bitstream_read(Res_bit[*ResL]) - Dc[*ResL];
                if (*ResR>7) *R++ = (int)Bitstream_read(Res_bit[*ResR]) - Dc[*ResR];
            }
    }
}



static const unsigned char    Parity [256] = {  // parity
    0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,
    1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,
    1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,
    0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,
    1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,
    0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,
    0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,
    1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0
};

static unsigned int  __r1 = 1;
static unsigned int  __r2 = 1;


unsigned int
random_int ( void )
{
#if 1
    unsigned int  t1, t2, t3, t4;

    t3   = t1 = __r1;   t4   = t2 = __r2;       // Parity calculation is done via table lookup, this is also available
    t1  &= 0xF5;        t2 >>= 25;              // on CPUs without parity, can be implemented in C and avoid unpredictable
    t1   = Parity [t1]; t2  &= 0x63;            // jumps and slow rotate through the carry flag operations.
    t1 <<= 31;          t2   = Parity [t2];

    return (__r1 = (t3 >> 1) | t1 ) ^ (__r2 = (t4 + t4) | t2 );
#else
    return (__r1 = (__r1 >> 1) | ((unsigned int)Parity [__r1 & 0xF5] << 31) ) ^
           (__r2 = (__r2 << 1) |  (unsigned int)Parity [(__r2 >> 25) & 0x63] );
#endif
}


/****************************************** SV 7 ******************************************/
void Lese_Bitstrom_SV7(void)
{
    static int idx30[] = { -1, 0, 1,-1, 0, 1,-1, 0, 1,-1, 0, 1,-1, 0, 1,-1, 0, 1,-1, 0, 1,-1, 0, 1,-1, 0, 1 };
    static int idx31[] = { -1,-1,-1, 0, 0, 0, 1, 1, 1,-1,-1,-1, 0, 0, 0, 1, 1, 1,-1,-1,-1, 0, 0, 0, 1, 1, 1 };
    static int idx32[] = { -1,-1,-1,-1,-1,-1,-1,-1,-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
    static int idx50[] = { -2,-1, 0, 1, 2,-2,-1, 0, 1, 2,-2,-1, 0, 1, 2,-2,-1, 0, 1, 2,-2,-1, 0, 1, 2 };
    static int idx51[] = { -2,-2,-2,-2,-2,-1,-1,-1,-1,-1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2 };

    int                n;
    int                k;
    int                Max_used_Band = 0;
    const HuffmanTyp*  Table;
    int                idx;
    int*               L;
    int*               R;
    int*               ResL;
    int*               ResR;
    unsigned int       tmp;

    /***************************** Header *****************************/
    ResL  = Res_L;
    ResR  = Res_R;

    // first subband
    *ResL = Bitstream_read(4);
    *ResR = Bitstream_read(4);
    if (MS_used && !(*ResL==0 && *ResR==0)) MS_Flag[0] = Bitstream_read(1);

    // consecutive subbands
    ++ResL; ++ResR; // increase pointers
    for (n=1; n<=Max_Band; ++n, ++ResL, ++ResR)
    {
        idx   = Huffman_Decode_fast(HuffHdr);
        *ResL = (idx!=4) ? *(ResL-1) + idx : Bitstream_read(4);

        idx   = Huffman_Decode_fast(HuffHdr);
        *ResR = (idx!=4) ? *(ResR-1) + idx : Bitstream_read(4);

        if (MS_used && !(*ResL==0 && *ResR==0)) MS_Flag[n] = Bitstream_read(1);

        // fuehre nachfolgende Operationen nur bis zum maximal enthaltenen Subband aus
        if (*ResL!=0 || *ResR!=0) Max_used_Band = n;
    }
    /****************************** SCFI ******************************/
    L     = SCFI_L;
    R     = SCFI_R;
    ResL  = Res_L;
    ResR  = Res_R;
    for (n=0; n<=Max_used_Band; ++n, ++L, ++R, ++ResL, ++ResR)
    {
        if (*ResL!=0) *L = Huffman_Decode_faster(HuffSCFI);
        if (*ResR!=0) *R = Huffman_Decode_faster(HuffSCFI);
    }

    /**************************** SCF/DSCF ****************************/
    ResL  = Res_L;
    ResR  = Res_R;
    L     = SCF_Index_L[0];
    R     = SCF_Index_R[0];
    for (n=0; n<=Max_used_Band; ++n, ++ResL, ++ResR, L+=3, R+=3)
    {
        if (*ResL != 0)
        {
            switch (SCFI_L[n])
            {
                case 1:
                    idx  = Huffman_Decode_fast(HuffDSCF);
                    L[0] = (idx!=8) ? DSCF_Reference_L[n] + idx : Bitstream_read(6);
                    idx  = Huffman_Decode_fast(HuffDSCF);
                    L[1] = (idx!=8) ? L[0]                + idx : Bitstream_read(6);
                    L[2] = L[1];
                    break;
                case 3:
                    idx  = Huffman_Decode_fast(HuffDSCF);
                    L[0] = (idx!=8) ? DSCF_Reference_L[n] + idx : Bitstream_read(6);
                    L[1] = L[0];
                    L[2] = L[1];
                    break;
                case 2:
                    idx  = Huffman_Decode_fast(HuffDSCF);
                    L[0] = (idx!=8) ? DSCF_Reference_L[n] + idx : Bitstream_read(6);
                    L[1] = L[0];
                    idx  = Huffman_Decode_fast(HuffDSCF);
                    L[2] = (idx!=8) ? L[1]                + idx : Bitstream_read(6);
                    break;
                default:
                    idx  = Huffman_Decode_fast(HuffDSCF);
                    L[0] = (idx!=8) ? DSCF_Reference_L[n] + idx : Bitstream_read(6);
                    idx  = Huffman_Decode_fast(HuffDSCF);
                    L[1] = (idx!=8) ? L[0]                + idx : Bitstream_read(6);
                    idx  = Huffman_Decode_fast(HuffDSCF);
                    L[2] = (idx!=8) ? L[1]                + idx : Bitstream_read(6);
                    break;
            }
            // update Reference for DSCF
            DSCF_Reference_L[n] = L[2];
        }
        if (*ResR != 0)
        {
            switch (SCFI_R[n])
            {
                case 1:
                    idx  = Huffman_Decode_fast(HuffDSCF);
                    R[0] = (idx!=8) ? DSCF_Reference_R[n] + idx : Bitstream_read(6);
                    idx  = Huffman_Decode_fast(HuffDSCF);
                    R[1] = (idx!=8) ? R[0]                + idx : Bitstream_read(6);
                    R[2] = R[1];
                    break;
                case 3:
                    idx  = Huffman_Decode_fast(HuffDSCF);
                    R[0] = (idx!=8) ? DSCF_Reference_R[n] + idx : Bitstream_read(6);
                    R[1] = R[0];
                    R[2] = R[1];
                    break;
                case 2:
                    idx  = Huffman_Decode_fast(HuffDSCF);
                    R[0] = (idx!=8) ? DSCF_Reference_R[n] + idx : Bitstream_read(6);
                    R[1] = R[0];
                    idx  = Huffman_Decode_fast(HuffDSCF);
                    R[2] = (idx!=8) ? R[1]                + idx : Bitstream_read(6);
                    break;
                default:
                    idx  = Huffman_Decode_fast(HuffDSCF);
                    R[0] = (idx!=8) ? DSCF_Reference_R[n] + idx : Bitstream_read(6);
                    idx  = Huffman_Decode_fast(HuffDSCF);
                    R[1] = (idx!=8) ? R[0]                + idx : Bitstream_read(6);
                    idx  = Huffman_Decode_fast(HuffDSCF);
                    R[2] = (idx!=8) ? R[1]                + idx : Bitstream_read(6);
                    break;
            }
            // update Reference for DSCF
            DSCF_Reference_R[n] = R[2];
        }
    }
    /***************************** Samples ****************************/
    ResL = Res_L;
    ResR = Res_R;
    L    = Q[0].L;
    R    = Q[0].R;
    for (n=0; n<=Max_used_Band; ++n, ++ResL, ++ResR, L+=36, R+=36)
    {
        /************** links **************/
        switch (*ResL)
        {
            case  -2: case  -3: case  -4: case  -5: case  -6: case  -7: case  -8: case  -9:
            case -10: case -11: case -12: case -13: case -14: case -15: case -16: case -17:
                L += 36;
                break;
            case -1:
                for (k=0; k<36; k++ ) {
                    tmp  = random_int ();
                    *L++ = ((tmp >> 24) & 0xFF) + ((tmp >> 16) & 0xFF) + ((tmp >>  8) & 0xFF) + ((tmp >>  0) & 0xFF) - 510;
                }
                break;
            case 0:
                L += 36;// increase pointer
                break;
            case 1:
                Table = HuffQ[Bitstream_read(1)][1];
                for (k=0; k<12; ++k)
                {
                    idx = Huffman_Decode_fast(Table);
                    *L++ = idx30[idx];
                    *L++ = idx31[idx];
                    *L++ = idx32[idx];
                }
                break;
            case 2:
                Table = HuffQ[Bitstream_read(1)][2];
                for (k=0; k<18; ++k)
                {
                    idx = Huffman_Decode_fast(Table);
                    *L++ = idx50[idx];
                    *L++ = idx51[idx];
                }
                break;
            case 3:
            case 4:
                Table = HuffQ[Bitstream_read(1)][*ResL];
                for (k=0; k<36; ++k) *L++ = Huffman_Decode_faster(Table);
                break;
            case 5:
                Table = HuffQ[Bitstream_read(1)][*ResL];
                for (k=0; k<36; ++k) *L++ = Huffman_Decode_fast(Table);
                break;
            case 6:
            case 7:
                Table = HuffQ[Bitstream_read(1)][*ResL];
                for (k=0; k<36; ++k) *L++ = Huffman_Decode(Table);
                break;
            case 8: case 9: case 10: case 11: case 12: case 13: case 14: case 15: case 16: case 17:
                for (k=0; k<36; ++k) *L++ = (int)Bitstream_read(Res_bit[*ResL]) - Dc[*ResL];
                break;
            default:
                return;
        }
        /************** rechts **************/
        switch (*ResR)
        {
            case  -2: case  -3: case  -4: case  -5: case  -6: case  -7: case  -8: case  -9:
            case -10: case -11: case -12: case -13: case -14: case -15: case -16: case -17:
                R += 36;
                break;
            case -1:
                for (k=0; k<36; k++ ) {
                    tmp  = random_int ();
                    *R++ = ((tmp >> 24) & 0xFF) + ((tmp >> 16) & 0xFF) + ((tmp >>  8) & 0xFF) + ((tmp >>  0) & 0xFF) - 510;
                }
                break;
            case 0:
                R += 36;// increase pointer
                break;
            case 1:
                Table = HuffQ[Bitstream_read(1)][1];
                for (k=0; k<12; ++k)
                {
                    idx = Huffman_Decode_fast(Table);
                    *R++ = idx30[idx];
                    *R++ = idx31[idx];
                    *R++ = idx32[idx];
                }
                break;
            case 2:
                Table = HuffQ[Bitstream_read(1)][2];
                for (k=0; k<18; ++k)
                {
                    idx = Huffman_Decode_fast(Table);
                    *R++ = idx50[idx];
                    *R++ = idx51[idx];
                }
                break;
            case 3:
            case 4:
                Table = HuffQ[Bitstream_read(1)][*ResR];
                for (k=0; k<36; ++k) *R++ = Huffman_Decode_faster(Table);
                break;
            case 5:
                Table = HuffQ[Bitstream_read(1)][*ResR];
                for (k=0; k<36; ++k) *R++ = Huffman_Decode_fast(Table);
                break;
            case 6:
            case 7:
                Table = HuffQ[Bitstream_read(1)][*ResR];
                for (k=0; k<36; ++k) *R++ = Huffman_Decode(Table);
                break;
            case 8: case 9: case 10: case 11: case 12: case 13: case 14: case 15: case 16: case 17:
                for (k=0; k<36; ++k) *R++ = (int)Bitstream_read(Res_bit[*ResR]) - Dc[*ResR];
                break;
            default:
                return;
        }
    }
}
