%define name xmms-musepack
%define version 0.94
%define release 1mdk
%define inst_dir %(xmms-config --plugin-dir)

Summary: This is an input plugin for XMMS which plays mp+ encoded audio files.
Name: %{name}
Version: %{version}
Release: %{release}
Copyright: GPL
Group: Sound
Source: http://prdownloads.sourceforge.net/mpegplus/%{name}-%{version}-%{release}.tar.bz2
Url: http://sourceforge.net/project/mpegplus/
Buildroot: %{_tmppath}/%{name}-buildroot

%description
This plugin for XMMS can play audio files which are encoded with Andree
Buschmann's encoder Musepack. These files have the filename postfixes mpc, mp+ or mpp.

%prep
rm -rf $RPM_BUILD_ROOT

%setup

%build
make CFLAGS="$RPM_OPT_FLAGS" all

%install
install -d $RPM_BUILD_ROOT%{inst_dir}/Input
install -s -m 0755 ./%{name}-%{version}.so $RPM_BUILD_ROOT%{inst_dir}/Input/%{name}-%{version}.so

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(0644,root,root,0755)
%doc ChangeLog README_mpp-plugin_eng.txt README_mpp-plugin_ger.txt
%defattr(0755,root,root,0755)
/%{inst_dir}/Input/%{name}-%{version}.so

%changelog
* Sun Aug 05 2001 Thomas Juerges <thomas.juerges@astro.ruhr-uni-bochum.de> 1.8.0-1mdk
- Upgraded to 1.8.0
- See ChangeLog
2002-05-10  Frank Klemm (pfk@uni-jena.de)

    * Version called 0.93

    * Fast seeking added

    * Fuzzy decoding added for low quality profile decoding
    
    * Bugfixed "Out of Sync" error, thanks Garf (but still strange - maybe XMMS or threadlib has bugs)

    * These are only hot fixes, future versions should be derived
      from the current WinAMP plugin


2002-05-05  Frank Klemm (pfk@uni-jena.de)

    * Version called 0.92

    * Gapless bugfix

    * Replaygain added

    * These are only hot fixes, future versions should be derived
      from the current WinAMP plugin


2001-08-05  Thomas Juerges <thomas.juerges@astro.ruhr-uni-bochum.de>

    * In_mpp.c: VERSION=1.8.0
        Removed the WINAMP stuff.

        Corrected an error in reading xmms config file.

        Completed evaluating the configuration and
        displaying of ID3 tags.

        Did some cosmetic changes, removed unnecessary
        definitions and variables.

    * The XMMS plugin seems to be complete now. Only
        a few bugs (if any ;) should remain.

    * Future releases (>=2.0.0) are planned to be set upon
        Frank Klemm's Athlon/P6 optimized decoder.
        Any helping hand to convert to his code is
        very appreciated.

2001-07-11  Thomas Juerges <thomas.juerges@astro.ruhr-uni-bochum.de>

    * Changed to name xmms-mpegplus.

    * Makefile: Changes name od library.
        Added Version.

    * In_mpp.c: Removed some ID3 handling which was too much.
        Things are already done in FileInfo and do not need
        to be done twice.


2001-06-17  Thomas Juerges <thomas.a.juerges@ruhr-uni-bochum.de>

    * xmms-mp+.spec: Added specfile for creating RPM archives.


2001-06-02  Thomas Juerges <thomas.a.juerges@ruhr-uni-bochum.de>

    * In_mpp.c: Displaying of ID3 tags is now o.k. They are
        now editable, too.

    I think, the plugin is pretty complete, now.


2001-06-02  Thomas Juerges <thomas.a.juerges@ruhr-uni-bochum.de>

    * In_mpp.c: Added logo of MP+ (very important :).

    * In_mpp.c: Tried to solve the seek problem with dynamic btrate
            display. Seems to work now.

    * In_mpp.c: Started implementing display of ID3 tags.


2001-05-28  Thomas Juerges <thomas.a.juerges@ruhr-uni-bochum.de>

    * In_mpp.c: Added configuration dialog.
        Supports selection of:
        - Continuous displaying / one tine displaying of
          the bitrate.
        - Switch clipping on / off.
    Removed a bug in saving ID3 tag information to the xmms-
    configuration file.

        Ready for the first production release 1.0.


2001-05-26  Thomas Juerges <thomas.a.juerges@ruhr-uni-bochum.de>

    * In_mpp.c: Added correct displaying of bitrate.
            Added support for seeking.
            Changed some text in the fileinfo dialog
            from German English to English English. ;)
            (we forgot some minor letters as ending "e"...)
                Corrected Andree's email address.
    Seems to work pretty cool now. :)


2001-04-23  Oliver Lemke  <oliver@uni-bremen.de>

    * In_mpp.c: Added support for files with mpc and mpp ending.


2001-04-16  Oliver Lemke  <oliver@uni-bremen.de>

    * ChangeLog: This file makes it easier to keep track of all changes.

    * Makefile: Added option -Wall.

    * In_mpp.c: Added xmms/util.h to include files.
      Changed the layout of the File Info Dlg.


2001-04-06   Oliver Lemke  <oliver@uni-bremen.de>

    * Fixed segmentation fault after playing one file.

    * Fixed segmentation fault when calling file info dialog while
        playing. Tested with xmms-2.4.0
    Email: olemke@soundfusion.de


2001-03-23   Thomas Juerges <thomas.a.juerges@ruhr-uni-bochum.de>

    * In_mpp.c: Updated to version 1.7.9


2000-02-17   Thomas Juerges <thomas.a.juerges@ruhr-uni-bochum.de>

    * added Linux support for xmms.
